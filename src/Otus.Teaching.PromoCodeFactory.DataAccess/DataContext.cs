﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preference { get; set; }
        public DbSet<PromoCode> PromoCode { get; set; }
        

        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerPreference>()
                .HasKey(x => new { x.CustomerId, x.PreferenceId });

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(x => x.Customer)
                .WithMany(x => x.Preferences)
                .HasForeignKey(x => x.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(x => x.Preference)
                .WithMany()
                .HasForeignKey(x => x.PreferenceId);
            
            modelBuilder.Entity<PromoCode>().HasKey(x => x.Id);

            modelBuilder.Entity<PromoCode>()
             .HasOne(x => x.Customer)
                .WithMany(x => x.PromoCodes)
                .OnDelete(DeleteBehavior.Cascade);
            
            modelBuilder.Entity<Role>().HasKey(x => x.Id);
            modelBuilder.Entity<Employee>().HasKey(x => x.Id);
            modelBuilder.Entity<Customer>().HasKey(x => x.Id);
            modelBuilder.Entity<Preference>().HasKey(x => x.Id);

            //modelBuilder.Entity<Customer>().Property(r => r.Email).IsRequired().HasMaxLength(52);

            

        }
    }
}
